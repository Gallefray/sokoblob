class sensor {
  float sensorPosX;
  float sensorPosY;
  float sensorSizeX;
  float sensorSizeY;
  float R;
  float G;
  float B;
  boolean sensorExists;

  sensor(float tempPosX, float tempPosY, float tempSizeX, float tempSizeY, color tempR, color tempG, color tempB) {
    sensorPosX = tempPosX;
    sensorPosY = tempPosY;
    sensorSizeX = tempSizeX;
    sensorSizeY = tempSizeY;
    R = tempR;
    G = tempG;
    B = tempB;
  }

  void disp() {
    if (sensorExists == true) {
      fill(R, G, B);
      rectMode(CENTER);
      rect(sensorPosX, sensorPosY, sensorSizeX, sensorSizeY);
    }
  }
}

