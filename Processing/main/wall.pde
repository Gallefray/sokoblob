class wall {
  float wallPosX;
  float wallPosY;
  float wallSizeX;
  float wallSizeY;
  float R;
  float G;
  float B;
  boolean wallExists;

  wall(float tempWallPosX, float tempWallPosY, float tempWallSizeX, float tempWallSizeY, color colourR, color colourG, color colourB) {
    wallPosX = tempWallPosX;
    wallPosY = tempWallPosY;
    wallSizeX = tempWallSizeX;
    wallSizeY = tempWallSizeY;
    R = colourR;
    G = colourG;
    B = colourB;
  }

  void disp() {
    if (wallExists == true) {
      fill(R, G, B);
      rectMode(CENTER);
      rect(wallPosX, wallPosY, wallSizeX, wallSizeY);
    }
  }


  void collide() {
    if (wallExists == true) {
      // these are the new coordinates for the box if we push it in
      // one of the four directions until it's not overlapping with the player
      float leftX = wallPosX - playersize/2 - wallSizeX/2;
      float rightX = wallPosX + playersize/2 + wallSizeX/2;
      float upY = wallPosY - playersize/2 - wallSizeY/2;
      float downY = wallPosY + playersize/2 + wallSizeY/2;

      // these are how many units the box would need to move in each direction
      // to not be overlapping with the player anymore

      float leftDX = playerX - leftX;
      float rightDX = rightX - playerX;
      float upDY = playerY - upY;
      float downDY = downY - playerY;


      // thanks to pekuja for the collision detection and collision handling code ::)
      // vv checks if circle - rect overlap
      if (abs(wallPosX-playerX) <= playersize/2 + wallSizeX/2 && abs(wallPosY-playerY) <= playersize/2 + wallSizeY/2) {
        println("box collision");

        // and here we pick the smallest one, so that we're moving the box
        // only the smallest amount required.
        if (leftDX <= rightDX && leftDX <= upDY && leftDX <= downDY) {
          playerX = leftX;
        }
        else if (rightDX <= leftDX && rightDX <= upDY && rightDX <= downDY) {
          playerX = rightX;
        }
        else if (upDY <= leftDX && upDY <= rightDX && upDY <= downDY) {
          playerY = upY;
        }
        else {
          playerY = downY;
        }
      }
    }
  }
}

