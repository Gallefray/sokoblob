float playerX = 400;
float playerY = 400;
float playersize = 50;

void playerdraw() {
  fill(0, 255, 0);
  ellipse(playerX, playerY, playersize, playersize);
}


float playerspeed = 4;
boolean playerGoingUp = false;
boolean playerGoingDown = false;
boolean playerGoingLeft = false;
boolean playerGoingRight = false;

//thanks to Nimphious for the overhaul of the movement (to make it work with the collision code) :)
void keyPressed() {
  // up / North
  if (key == 'w') {
    playerGoingUp = true;
    playerGoingDown = false;
    playerGoingLeft = false;
    playerGoingRight = false;
  } 
  // down / South
  if (key == 's') {
    playerGoingUp = false;
    playerGoingDown = true;
    playerGoingLeft = false;
    playerGoingRight = false;
  }
  // left / West
  if (key == 'a') {
    playerGoingUp = false;
    playerGoingDown = false;
    playerGoingLeft = true;
    playerGoingRight = false;
  }
  // right / East
  if (key == 'd') {
    playerGoingUp = false;
    playerGoingDown = false;
    playerGoingLeft = false;
    playerGoingRight = true;
  }
}

void keyReleased() {
  // up / North
  if (key == 'w') {
    playerGoingUp = false;
  } 
  // down / South
  if (key == 's') {
    playerGoingDown = false;
  }
  // left / West
  if (key == 'a') {
    playerGoingLeft = false;
  }
  // right / East
  if (key == 'd') {
    playerGoingRight = false;
  }
}

void PlayerMove() {
  // up / North
  if (playerGoingUp == true) {
    playerY-=playerspeed;
  } 
  // down / South
  if (playerGoingDown == true) {
    playerY+=playerspeed;
  }
  // left / West
  if (playerGoingLeft == true) {
    playerX-=playerspeed;
  }
  // right / East
  if (playerGoingRight == true) {
    playerX+=playerspeed;
  }    
}


void playerEdgeCollide() {
  if (playerX-25 <= 0) {
    playerX+=playerspeed;
  }
  else if (playerX+25 >= 800) {
    playerX-=playerspeed;
  }
  else if (playerY-25 <= 0) {
    playerY+=playerspeed;
  }
  else if (playerY+25 >= 600) {
    playerY-=playerspeed;
  }
}

