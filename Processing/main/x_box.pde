class boxx {
  float boxPosX;
  float boxPosY;
  float boxSizeX;
  float boxSizeY;
  float R;
  float G;
  float B;
  boolean boxExists;

  boxx(float tempBoxPosX, float tempBoxPosY, float tempBoxSizeX, float tempBoxSizeY, color colourR, color colourG, color colourB) {
    boxPosX = tempBoxPosX;
    boxPosY = tempBoxPosY;
    boxSizeX = tempBoxSizeX;
    boxSizeY = tempBoxSizeY;
    R = colourR;
    G = colourG;
    B = colourB;
  }

  void disp() {
    if (boxExists == true) {
      fill(R, G, B);
      rectMode(CENTER);
      rect(boxPosX, boxPosY, boxSizeX, boxSizeY);
    }
  }

  void collide() {
    if (boxExists == true) {
      /**
       Inventory of box.collide()
       item one: box collision with wall
       item two: player collision with box
       **/
//ITEM ONE:
        if (boxPosX-25 <= 0) {
          boxPosX+=playerspeed;
        }
        else if (boxPosX+25 >= 800) {
          boxPosX-=playerspeed;
        }
        else if (boxPosY-25 <= 0) {
          boxPosY+=playerspeed;
        }
        else if (boxPosY+25 >= 600) {
          boxPosY-=playerspeed;
        }

// ITEM TWO:      
      // these are the new coordinates for the box if we push it in
      // one of the four directions until it's not overlapping with the player
      float leftX = playerX - playersize/2 - boxSizeX/2;
      float rightX = playerX + playersize/2 + boxSizeX/2;
      float upY = playerY - playersize/2 - boxSizeY/2;
      float downY = playerY + playersize/2 + boxSizeY/2;

      // these are how many units the box would need to move in each direction
      // to not be overlapping with the player anymore
      float leftDX = boxPosX - leftX;
      float rightDX = rightX - boxPosX;
      float upDY = boxPosY - upY;
      float downDY = downY - boxPosY;


      // thanks to pekuja for the collision detection code ::)
      // vv checks if circle - rect overlap
      if (abs(boxPosX-playerX) <= playersize/2 + boxSizeX/2 && abs(boxPosY-playerY) <= playersize/2 + boxSizeY/2) {
        println("box collision");
        // and here we pick the smallest one, so that we're moving the box
        // only the smallest amount required.
        if (leftDX <= rightDX && leftDX <= upDY && leftDX <= downDY) {
          boxPosX = leftX;
        }
        else if (rightDX <= leftDX && rightDX <= upDY && rightDX <= downDY) {
          boxPosX = rightX;
        }
        else if (upDY <= leftDX && upDY <= rightDX && upDY <= downDY) {
          boxPosY = upY;
        }
        else {
          boxPosY = downY;
        }
      }
    }
  }
}

