/** all code is the property of Polyhedron Design Studios and is licensed under the zlib license **/
int windowX = 800;
int windowY = 600;


wall wall1;
wall wall2;
boxx box1;
sensor sensor1;

void setup() {
  frame.setTitle("Sokoban Clone");
  size(windowX, windowY);
                // X,  Y,  W,  H,  R,  G,  B;
  wall1 = new wall(50, 50, 50, 50, 0, 255, 0);
  wall2 = new wall(150, 150, 200, 200, 255, 0, 0);
  box1 = new boxx(300, 300, 49, 49, 0, 0, 255);
  box1 = new boxx(300, 300, 50, 50, 0, 0, 255);
  sensor1 = new sensor(500, 500, 50, 50, 50, 50, 50);
}

void draw() {
  background(165, 165, 165);
  showEntitys();
  playerdraw();
  PlayerMove();
  playerEdgeCollide();
}

void showEntitys() {
  wall1.wallExists = false;
  wall1.disp();
  wall1.collide();

  wall2.wallExists = true;
  wall2.disp();
  wall2.collide();

  box1.boxExists = true;
  box1.disp();
  box1.collide();

  sensor1.sensorExists = false;
  sensor1.disp();
}

void sensorboxcoll() {
}

