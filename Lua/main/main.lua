--[[
 > = greater than
 < = less than
]]

-- importing libs
--[HC = require 'hardoncollider']

function love.load() -- in processing this was "void setup"
	loadatsetup()
end

function love.update(dt)
 -- the code to make the player move 
 if love.keyboard.isDown("left") then
    hero.x = hero.x - hero.speed
 elseif love.keyboard.isDown("right") then
    hero.x = hero.x + hero.speed
 elseif love.keyboard.isDown("up") then
    hero.y = hero.y - hero.speed
 elseif love.keyboard.isDown("down") then
    hero.y = hero.y + hero.speed
 -- and some other keyboard handling
 elseif love.keyboard.isDown("escape") then -- to exit the program, press escape
 	love.event.quit()
 end

 playercollidewithwall()
end

function love.draw() -- in processing this was "void draw"
 drawentitys()
end

--  ==========================| Creating And Displaying The Entitys |==========================

function drawentitys()
	 -- let's draw our hero
     love.graphics.setColor(0,0,255,255)
     love.graphics.circle("fill", hero.x, hero.y, hero.radius)

     -- lets draw the box
     love.graphics.setColor(255,255,0,255)
     love.graphics.rectangle("fill", box.x, box.y, box.x, box.y)
end

function loadatsetup()
 --[index: 1.table.hero, 2.table.box]

 -- 1.table.hero:
 hero = {} -- new table for the hero
 hero.x = 300 
 hero.y = 450
 hero.speed = 10
 hero.radius = 30
 
 -- 2.table.box
 box = {}
 box.x = 500
 box.y = 650
 box.speed = 100
 box.x = 50
 box.y = 50
end

--  ==========================| Collision Code Below |==========================

function playercollidewithwall()
	 -- check if the player collides with the edge of the screen/window along the x coordinates
  -- if playerX >= right edge of window... 
	 if hero.x >= 1024 then
	 	hero.x = hero.x - 10
	 elseif hero.x <= 0 then
	 	hero.x = hero.x + 10
	 end

	 -- check if the player collides with the edge of the screen/window along the y coordinates
  -- if playerX >= bottom edge of window... 
	 if hero.y >= 768 then
	 	hero.y = hero.y - 10
	 elseif hero.y <= 0 then
	 	hero.y = hero.y + 10
	 end
end

--  ==========================| EOF (End Of File) |==========================