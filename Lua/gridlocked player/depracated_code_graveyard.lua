--[[
function handleCollision()
    if testplayerbox(true) then
        if testMapBox(0, -1) and testMapPlayer(0, -1) then     -- up
            box.grid_y = box.grid_y - 32
        elseif testMapBox(0, 1) and testMapPlayer(0, 1) then  -- down
            box.grid_y = box.grid_y + 32
        elseif testMapBox(-1, 0) and testMapPlayer(-1, 0) then -- left
            box.grid_x = box.grid_x - 32
        elseif testMapBox(1, 0) and testMapPlayer(1, 0) then  -- right
            box.grid_x = box.grid_x + 32
        end
    end
end
]]

--love.timer.sleep(3)

--[[

if testPlayerBox() == true then
        -- The conversion's not pretty, but it should work... - Gallefray
        -- Adapted from processing (Pekuja's code):

        -- these are the new coordinates for the box if we push it in
        -- one of the four directions until it's not overlapping with the player - Pekuja
        leftX = player.grid_x - 32/2 - 32/2
        rightX = player.grid_x + 32/2 + 32/2
        upY = player.grid_y - 32/2 - 32/2
        downY = player.grid_y + 32/2 + 32/2

        -- these are how many units the box would need to move in each direction
        -- to not be overlapping with the player anymore - Pekuja
        leftDX = math.abs(box.grid_x - leftX)
        rightDX = box.grid_x - rightX
        upDY = box.grid_y - upY
        downDY = math.abs(box.grid_y - downY)

        -- and here we pick the smallest one, so that we're moving the box
        -- only the smallest amount required - Pekuja
        if leftDX <= rightDX and leftDX <= upDY and leftDX <= downDY then
            if testMapPlayer(-1,0) then
                box.grid_x = leftX
            end
        elseif rightDX <= leftDX and rightDX <= upDY and rightDX <= downDY then
            if testMapPlayer(1,0) then
                box.grid_x = rightX
            end
        elseif upDY <= leftDX and upDY <= rightDX and upDY <= downDY then
            if testMapPlayer(0,-1) then
                box.grid_y = upY
            end
        else
            if testMapPlayer(0,1) then
                box.grid_y = downY
            end
        end
    end
]]

--if player.act_x > box.act_x and player.act_x < box.act_x + 32 and player.act_y > box.act_y and player.act_y < box.act_y + 32 then








--[[

    if player.big == false then
        if map.currentLevel == 1 then
            if map.level1[math.floor((player.grid_y / player.size_y) + y)][math.floor((player.grid_x / player.size_x) + x)] == 1 then
                return false
            end
        elseif map.currentLevel == 2 then
            if map.level2[math.floor((player.grid_y / player.size_y) + y)][(math.floor(player.grid_x / player.size_x) + x)] == 1 then
                return false
            end
        elseif map.currentLevel == 3 then
            if map.level3[math.floor((player.grid_y / player.size_y) + y)][(math.floor(player.grid_x / player.size_x) + x)] == 1 then
                return false
            end
        elseif map.currentLevel == 100 then
            if map.level100[math.floor((player.grid_y / player.size_y) + y)][(math.floor(player.grid_x / player.size_x) + x)] == 1 then
                return false
            end
        end
    end

]]

    -- love.graphics.print("Press 'Esc' or 'q' to quit at any time", 421, 395)
    -- love.graphics.print("use w, a, s, d or the arrow keys to move", 421, 415)




-- -- special movement keys (the player will carry on in the given direction until an obstruction is reached)
--         while love.keyboard.isDown("lshift") and love.keyboard.isDown("up") or love.keyboard.isDown("lshift") and love.keyboard.isDown("w") do
--             if testMapPlayer(0, -1) then
--                 player.move_direction = "up"
--                 player.gY = player.gY - 32
--             else 
--                 break
--             end
--         end
--         while love.keyboard.isDown("lshift") and love.keyboard.isDown("down") or love.keyboard.isDown("lshift") and love.keyboard.isDown("s") do
--             if testMapPlayer(0, 1) then
--                 player.move_direction = "down"
--                 player.gY = player.gY + 32
--             else
--                 break
--             end
--         end
--         while love.keyboard.isDown("lshift") and love.keyboard.isDown("left") or love.keyboard.isDown("lshift") and love.keyboard.isDown("a") do
--             if testMapPlayer(-1, 0) then
--                 player.move_direction = "left"
--                 player.gX = player.gX - 32
--             else
--                 break
--             end
--         end
--         while love.keyboard.isDown("lshift") and love.keyboard.isDown("right") or love.keyboard.isDown("lshift") and love.keyboard.isDown("d") do
--             if testMapPlayer(1, 0) then
--                 player.move_direction = "right"
--                 player.gX = player.gX + 32
--             else
--                 break
--             end
--         end


    -- map.level1 = {
    -- -- 30 rows: -- , 23 columns: | (it now fills the screen)                                           
    -- { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    -- { 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1},
    -- { 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},
    -- { 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1},
    -- { 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1},
    -- { 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1},
    -- { 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    -- }
    -- map.level2 = {
    -- -- 30 rows, 23 columns (it now fills the screen)                                           
    -- { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    -- { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    -- }