--how to fake classes in lua
do
    local function call(cls, ...)
        instance = setmetatable({}, cls)
        if cls.__init then
            cls.__init(instance, ...)
        end
        return instance
    end
 
    function class()
        local cls = {}
        cls.__index = cls
        return setmetatable(cls, {__call = call})
    end
end