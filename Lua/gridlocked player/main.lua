--[[
 > = greater than
 < = less than
]]


-- the error is somewhere around testPlayerBox(), handlePlayerBox(), testPlayerPellet() and handlePlayerPellet()

-- importing libs (var = require 'NameOfFile')
mappum = require 'maps'
classum = require 'classes'

-- I'm in Space!(tm)(c)

function resources()
	-- importing resources
    pixelfont = love.graphics.newFont("data/munro_small.ttf", 25) -- epic pixxley font!
    normal_ish = love.graphics.newFont("data/akashi.ttf", 35) -- a blend between the star wars and star trek font
    normalish_smaller = love.graphics.newFont("data/akashi.ttf", 23)
    walltile = love.graphics.newImage("data/wall.png")
    doortile = love.graphics.newImage("data/door.png")
    watertile = love.graphics.newImage("data/water.png")
end


function love.load()
    blockReset = false -- allows for new instances of classes (water... etc) to be init'ed at the start of each level (blockPos)
    loadClassFunctions() -- loads the classes
    mapsloadatsetup() -- loads the maps
	resources() -- annd finally the resources
end

function love.update(dt)
    -- this may cause some pain later on... but for now I have to do this D: : 
    if map.currentLevel == 101 then 
        love.event.quit()
    end

    blockPos() -- resets the blocks positions at the start of each level
    -- actual_x = actual_x - (actual_x - destination_x)
    if map.currentLevel > 0 then
        player.aY = player.aY - ((player.aY - player.gY) * player.vel * dt)
        player.aX = player.aX - ((player.aX - player.gX) * player.vel * dt)

        levelexit1.aY = levelexit1.aY - ((levelexit1.aY - levelexit1.gY) * levelexit1.vel * dt)
        levelexit1.aX = levelexit1.aX - ((levelexit1.aX - levelexit1.gX) * levelexit1.vel * dt)

        if map.currentLevel == 3 then
            box1.aY = box1.aY - ((box1.aY - box1.gY) * box1.vel * dt)
            box1.aX = box1.aX - ((box1.aX - box1.gX) * box1.vel * dt)

            box2.aY = box2.aY - ((box2.aY - box2.gY) * box2.vel * dt)
            box2.aX = box2.aX - ((box2.aX - box2.gX) * box2.vel * dt)

            sensor1.aY = sensor1.aY - ((sensor1.aY - sensor1.gY) * sensor1.vel * dt)
            sensor1.aX = sensor1.aX - ((sensor1.aX - sensor1.gX) * sensor1.vel * dt)

            sensor2.aY = sensor2.aY - ((sensor2.aY - sensor2.gY) * sensor2.vel * dt)
            sensor2.aX = sensor2.aX - ((sensor2.aX - sensor2.gX) * sensor2.vel * dt)

            door1.aY = door1.aY - ((door1.aY - door1.gY) * door1.vel * dt)
            door1.aX = door1.aX - ((door1.aX - door1.gX) * door1.vel * dt)

            door2.aY = door2.aY - ((door2.aY - door2.gY) * door2.vel * dt)
            door2.aX = door2.aX - ((door2.aX - door2.gX) * door2.vel * dt)
        end
    end
end

function love.draw()
    mapselect() -- this selects and draws the map depending on map.currentLevel = number
    drawentitys() -- does what it says on the box
    UponDeath() -- death screen
    Menu() -- loads a menu screen if the map number is 0
end

function love.keypressed()
    if map.currentLevel == 0 then -- menu keyboard handling :D
        -- to start a new game:
        if love.keyboard.isDown("p") then
            blockReset = false
            map.currentLevel = map.currentLevel + 1
        end
    end
    if map.currentLevel > 0 then
        -- the code to make the player move 
        if love.keyboard.isDown("up") or love.keyboard.isDown("w") then
            if testMapPlayer(0, -1) then
                player.move_direction = "up"
                player.gY = player.gY - 32
            end
        end
        if love.keyboard.isDown("down") or love.keyboard.isDown("s") then
            if testMapPlayer(0, 1) then
                player.move_direction = "down"
                player.gY = player.gY + 32
            end
        end
        if love.keyboard.isDown("left") or love.keyboard.isDown("a") then
            if testMapPlayer(-1, 0) then
                player.move_direction = "left"
                player.gX = player.gX - 32
            end
        end
        if love.keyboard.isDown("right") or love.keyboard.isDown("d") then
            if testMapPlayer(1, 0) then
                player.move_direction = "right"
                player.gX = player.gX + 32
            end
        end
        -- level reset button
        if love.keyboard.isDown("r") and map.currentLevel > 0 then
            if screenBlurr == true then
                screenBlurr = false
            end
            blockReset = false
            player.move_direction = "neutral"
        end
    end
    -- and some other keyboard handling
    if love.keyboard.isDown("escape") or love.keyboard.isDown("q") then -- to exit the program, press escape
        love.event.quit()
    end
end

--  ==========================| Creating And Displaying The Entitys |==========================
function loadClassFunctions() -- TODO: write a load of puns about classes here :D
    hero = class()
    box = class()
    sensor = class()
    levelexit = class()
    door = class()
    water = class()

    function hero:__init(grid_x, grid_y, act_x, act_y, speed)
        --if a function named __init exists, it will be called when an object is initialized (like a constructor)
        self.gX = grid_x
        self.gY = grid_y
        self.aX = act_x
        self.aY = act_y
        self.vel = speed
        self.move_direction = "neutral"
    end

-- box classes, each of these has to be called individually when a new box is called, although it does save time :)

    function box:__init(grid_x, grid_y, act_x, act_y, speed)
        self.gX = grid_x
        self.gY = grid_y
        self.aX = act_x
        self.aY = act_y
        self.vel = speed
        self.move_direction = "neutral"
    end

    function box:draw(X, Y)
        love.graphics.setColor(0, 0, 255, 255)    
        love.graphics.draw(walltile, X, Y)       
    end

    function box:maptest(x, y)
        print("maptest code is running")
        if map.currentLevel == 1 then
            if map.level1[math.floor((self.gY / 32) + y)][math.floor((self.gX / 32) + x)] == 1 then
                return false
            end
        elseif map.currentLevel == 2 then
            if map.level2[math.floor((self.gY / 32) + y)][math.floor((self.gX / 32) + x)] == 1 then
                return false
            end
        elseif map.currentLevel == 3 then
            if map.level3[math.floor((self.gY / 32) + y)][math.floor((self.gX / 32) + x)] == 1 then
                return false
            end
        elseif map.currentLevel == 100 then
            if map.level100[math.floor((self.gY / 32) + y)][math.floor((self.gX / 32) + x)] == 1 then
                return false
            end
        end
        return true
    end

    function box:playerCollide(player)
        playerboxcollide = false 
        if math.abs(self.gX - player.gX) <= 32/2 + 32/2 and math.abs(self.gY - player.gY) <= 32/2 + 32/2 then
             playerboxcollide = true
             print("playerboxcollide is true")
        else
             playerboxcollide = false
             print("playerboxcollide is false")
        end

        if playerboxcollide == true then
            print("player-box collision handling code is running")
            if player.move_direction == "up" then
                self.move_direction = "up"
            elseif player.move_direction == "down" then
                self.move_direction = "down"
            elseif player.move_direction == "left" then
                self.move_direction = "left"
            elseif player.move_direction == "right" then
                self.move_direction = "right"
            end
        end
    end

    function box:react()
        if self.move_direction == "up" then
            if self:maptest(0, -1) then
                self.gY = self.gY - 32
                self.move_direction = "neutral"
            end
        elseif self.move_direction == "down" then
            if self:maptest(0, 1) then
                self.gY = self.gY + 32
                self.move_direction = "neutral"
            end
        elseif self.move_direction == "left" then
            if self:maptest(-1, 0) then
                self.gX = self.gX - 32
                self.move_direction = "neutral"
            end
        elseif self.move_direction == "right" then
            if self:maptest(1, 0) then
                self.gX = self.gX + 32
                self.move_direction = "neutral"
            end
        end
    end

-- sensor classes
    function sensor:__init(grid_x, grid_y, act_x, act_y, speed)
        self.gX = grid_x
        self.gY = grid_y
        self.aX = act_x
        self.aY = act_y
        self.vel = speed
    end

    function sensor:draw(X, Y)
        love.graphics.setColor(0, 255, 0, 255)  
        love.graphics.draw(walltile, X, Y)
    end


    function sensor:boxCollide(box, doorname)
        boxsensorcollide = false
        -- testing code (works fine) 
        if math.abs(self.gX - box.gX) <= 32/2 + 32/2 and math.abs(self.gY - box.gY) <= 32/2 + 32/2 then
             boxsensorcollide = true
             print("boxsensorcollide is true")
        else
             boxsensorcollide = false
             print("boxsensorcollide is false")
        end

        if boxsensorcollide == true then
            print("box-sensor collision handling code is running")
            doorname.unlocked = true -- it's not passing out this line?
        elseif boxsensorcollide == false then
            doorname.unlocked = false
        end
    end

-- levelexit classes
    function levelexit:__init(grid_x, grid_y, act_x, act_y, speed)
        self.gX = grid_x
        self.gY = grid_y
        self.aX = act_x
        self.aY = act_y
        self.vel = speed
    end

    function levelexit:draw(X, Y)
        love.graphics.setColor(255, 255, 0, 255)  
        love.graphics.draw(walltile, X, Y)
    end


    function levelexit:playerCollide(player)
        playerexitcollide = false
        -- testing code (works fine) 
        if math.abs(self.gX - player.gX) <= 32/2 + 32/2 and math.abs(self.gY - player.gY) <= 32/2 + 32/2 then
             playerexitcollide = true
             print("playerExitCollide is true")
        else
             playerexitcollide = false
             print("playerExitCollide is false")
        end

        if playerexitcollide == true then
            print("player-exit collision handling code is running")
            map.currentLevel = map.currentLevel + 2
            blockReset = false -- I need to reset the position of the player, 
            -- or the player ends up in the world between worlds! D:
            -- MWhahahahahahahahahahahahahahahahahahahahahahahaaa!
        end
    end

-- door classes
    function door:__init(grid_x, grid_y, act_x, act_y, speed, status)
        self.gX = grid_x
        self.gY = grid_y
        self.aX = act_x
        self.aY = act_y
        self.vel = speed
        self.unlocked = status
    end

    function door:draw(X, Y)
        if self.unlocked == true then
            love.graphics.setColor(255, 0, 255, 0)
        elseif self.unlocked == false then
            love.graphics.setColor(255, 0, 255, 255)
        end
        love.graphics.draw(doortile, X, Y)
    end


    function door:playerCollide(player)
        playerdoorcollide = false
        -- testing code (works fine) 
        if math.abs(self.gX - player.gX) <= 32/2 + 32/2 and math.abs(self.gY - player.gY) <= 32/2 + 32/2 then
             playerdoorcollide = true
             print("playerDoorCollide is true")
        else
             playerdoorcollide = false
             print("playerDoorCollide is false")
        end

        if playerdoorcollide == true and self.unlocked == false then
            print("player-door collision handling code is running")
            if player.move_direction == "up" then
                player.gY = player.gY + 32
            elseif player.move_direction == "down" then
                player.gY = player.gY - 32
            elseif player.move_direction == "left" then
                player.gX = player.gX + 32
            elseif player.move_direction == "right" then
                player.gX = player.gX - 32
            end
        end
    end

    function door:boxCollide(box)
        boxdoorcollide = false
        -- testing code (works fine) 
        if math.abs(self.gX - box.gX) <= 32/2 + 32/2 and math.abs(self.gY - box.gY) <= 32/2 + 32/2 then
             boxdoorcollide = true
             print("boxDoorCollide is true")
        else
             boxdoorcollide = false
             print("boxDoorCollide is false")
        end

        if boxdoorcollide == true and self.unlocked == false then
            print("box-door collision handling code is running")
            if box.move_direction == "up" then
                box.gY = box.gY + 32
            elseif box.move_direction == "down" then
                box.gY = box.gY - 32
            elseif box.move_direction == "left" then
                box.gX = box.gX + 32
            elseif player.move_direction == "right" then
                box.gX = box.gX - 32
            end
        end
    end

-- water classes
    function water:__init(grid_x, grid_y, act_x, act_y, speed)
        self.gX = grid_x
        self.gY = grid_y
        self.aX = act_x
        self.aY = act_y
        self.vel = speed
    end

    function water:draw(X, Y)
        love.graphics.setColor(150, 255, 255, 255)
        love.graphics.draw(watertile, X, Y)
    end

    function water:playerCollide(player)
        playerwatercollide = false
        -- testing code (works fine) 
        if math.abs(self.gX - player.gX) <= 32/2 + 32/2 and math.abs(self.gY - player.gY) <= 32/2 + 32/2 then
             playerwatercollide = true
             print("playerWaterCollide is true")
        else
             playerwatercollide = false
             print("playerWaterCollide is false")
        end

        if playerwatercollide == true then
            print("player-water collision handling code is running")
            screenBlurr = true -- death code
        end
    end
end


--  ==========================| Menu Code |==========================

function Menu() -- DO NOT TOUCH! IT IS PERFECT!
    if map.currentLevel == 0 then
        love.graphics.setFont(normal_ish)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.print("Sokoblob", 340, 320) 
        love.graphics.setFont(pixelfont)
        love.graphics.print("Press 'p' to start a new game", 421, 375) -- p for play
        love.graphics.print("use w, a, s, d or the arrow keys to move", 421, 395)
        love.graphics.print("Press 'r' to reset the current level", 421, 415)
        love.graphics.print("Press 'Esc' or 'q' to quit at any time", 421, 435)
    end
end
 
function UponDeath()
    if screenBlurr == true then
        love.graphics.setColor(0, 0, 0, 200)
        love.graphics.rectangle("fill", 0, 0, 1024, 768)
        love.graphics.setFont(normalish_smaller)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.print("TestSubject06 shall be sorely missed... not! AHAHAHA", 160, 320)
        love.graphics.setFont(pixelfont)
        love.graphics.print("Press 'r' to reload the level", 345, 370) -- DO NOT TOUCH! IT IS PERFECT!
        blockReset = false
    end 
end

-- PEACE OF MIND LOG:
-- all of the above code works (to my knowledge) as of 22/10/12 16:30
-- all of the above code works (to my knowledge) as of 22/10/12 17:02
-- all of the above code mostly works as of 22/10/12 17:35
-- all of the above code works (to my knowledge) as of 23/10/12 13:02
-- all of the above code works (to my knowledge) as of 25/10/12 15:12
-- all of the above code works (to my knowledge) as of 26/10/12 12:05 with no apparent bugs ::: committed

----------- 25 Oct 2012:
-- Added:
-- a menu
-- a new menu entry (esc)
-- fonts!
-- sorted out a bug that caused entity positions to 
-- continually reset in maps that have  higher designated number than 1
-- replaced "load at setup" with blockPos

----------- 26 Oct 2012:
-- Added:
-- w, a, s, d for movement
-- q as quit
-- door - box collisions
-- ^ overhauled the box's movement system
-- level 3 is done! I shall probably redesignate it soon :)
-- 17:27 : the player can now press "r" at any time and it will reset the current level

-- forgot to log progress at this point...

----------- 28 Oct 2012:
-- Added:
-- most of the code in void update :D
-- the first tutorial level

----------- 29 Oct 2012:




























