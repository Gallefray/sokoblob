--[[
 > = greater than
 < = less than
]]


-- the error is somewhere around testPlayerBox(), handlePlayerBox(), testPlayerPellet() and handlePlayerPellet()

-- importing libs (var = require 'NameOfFile')
mappum = require 'maps'

-- I'm in Space!(tm)(c)

function resources()
	-- importing resources
    walltile = love.graphics.newImage("data/wall.png")
    walltilebig = love.graphics.newImage("data/wall_big.png")
    doortile = love.graphics.newImage("data/door.png")
end


function love.load() -- in processing this was "void setup"
	loadatsetup()
    mapsloadatsetup()
	resources()
end

function love.update(dt)
    -- this may cause some pain later on... but for now I have to do this D: : 
    if map.currentLevel == 101 then 
        love.event.quit()
    end

    mapcheck() -- make sures that the player doesn't go into black space ;)


    -- actual_x = actual_x - (actual_x - destination_x)
    
    player.act_y = player.act_y - ((player.act_y - player.grid_y) * player.speed * dt)
    player.act_x = player.act_x - ((player.act_x - player.grid_x) * player.speed * dt)

    box.act_y = box.act_y - ((box.act_y - box.grid_y) * box.speed * dt)
    box.act_x = box.act_x - ((box.act_x - box.grid_x) * box.speed * dt)

    levelexit.act_y = levelexit.act_y - ((levelexit.act_y - levelexit.grid_y) * levelexit.speed * dt)
    levelexit.act_x = levelexit.act_x - ((levelexit.act_x - levelexit.grid_x) * levelexit.speed * dt)

    sensor.act_y = sensor.act_y - ((sensor.act_y - sensor.grid_y) * sensor.speed * dt)
    sensor.act_x = sensor.act_x - ((sensor.act_x - sensor.grid_x) * sensor.speed * dt)

    door.act_y = door.act_y - ((door.act_y - door.grid_y) * door.speed * dt)
    door.act_x = door.act_x - ((door.act_x - door.grid_x) * door.speed * dt)

    pellet.act_y = pellet.act_y - ((pellet.act_y - pellet.grid_y) * pellet.speed * dt)
    pellet.act_x = pellet.act_x - ((pellet.act_x - pellet.grid_x) * pellet.speed * dt)
end

function love.draw() -- in processing this was "void draw"
    drawentitys()
    CollisionExecution() -- Executes all of the collision Code (testing & handling)
    playerBigExpires()
    mapselect() -- this selects and draws the map depending on map.currentLevel = number
end

function love.keypressed()
    -- the code to make the player move 
    if love.keyboard.isDown("up") then
        if testMapPlayer(0, -1) then
            player.move_direction = "up"
            player.grid_y = player.grid_y - 32
            if player.startTurnCount == true then
                player.turnsSinceSmall = player.turnsSinceSmall + 1
            end
        end
    end
    if love.keyboard.isDown("down") then
        if testMapPlayer(0, 1) then
            player.move_direction = "down"
            player.grid_y = player.grid_y + 32
            if player.startTurnCount == true then
                player.turnsSinceSmall = player.turnsSinceSmall + 1
            end
        end
    end
    if love.keyboard.isDown("left") then
        if testMapPlayer(-1, 0) then
            player.move_direction = "left"
            player.grid_x = player.grid_x - 32
            if player.startTurnCount == true then
                player.turnsSinceSmall = player.turnsSinceSmall + 1
            end
        end
    end
    if love.keyboard.isDown("right") then
        if testMapPlayer(1, 0) then
            player.move_direction = "right"
            player.grid_x = player.grid_x + 32
            if player.startTurnCount == true then
                player.turnsSinceSmall = player.turnsSinceSmall + 1
            end
        end
    end
    -- and some other keyboard handling
    if love.keyboard.isDown("escape") then -- to exit the program, press escape
        love.event.quit()
    end
end

--  ==========================| Creating And Displaying The Entitys |==========================
function drawentitys()
     love.graphics.setColor(255, 255, 0, 255)
     love.graphics.draw(walltile, levelexit.grid_x, levelexit.grid_y) -- draws the level exit

     love.graphics.setColor(0, 255, 0, 255)
     love.graphics.draw(walltile, sensor.grid_x, sensor.grid_y) -- draws the sensor

     if pellet.exists == true then
        love.graphics.setColor(0, 255, 255, 255)
        love.graphics.draw(walltile, pellet.grid_x, pellet.grid_y) -- draws the pellet
     end

     if door.unlocked == true then
        love.graphics.setColor(255, 0, 255, 0)
     elseif door.unlocked == false then
        love.graphics.setColor(255, 0, 255, 255)
     end
     love.graphics.draw(doortile, door.grid_x, door.grid_y) -- draws the door


     if player.big == false then
        player.size_x = 32
        player.size_y = 32
        love.graphics.setColor(255, 0, 0, 255)    
        love.graphics.draw(walltile, player.grid_x, player.grid_y) -- draws the player
     elseif player.big == true then
        player.size_x = 64
        player.size_y = 64
        love.graphics.setColor(255, 0, 0, 255)    
        love.graphics.draw(walltilebig, player.grid_x, player.grid_y) -- draws the player
     end

     if box.big == false then
        box.size_x = 32
        box.size_y = 32
        love.graphics.setColor(0, 0, 255, 255)
        love.graphics.draw(walltile, box.grid_x, box.grid_y) -- draws the blue box
     elseif box.big == true then
        box.size_x = 64
        box.size_y = 64
        love.graphics.setColor(0, 0, 255, 255)
        love.graphics.draw(walltilebig, box.grid_x, box.grid_y)
     end

end

function loadatsetup()
 --[[
 
 index: 
 1.table.player, 2.table.box, 3.table.levelexit, 4.table.sensor, 5.table.door, 6.table.pellet

 ]]

    -- 1.table.player:
    player = {} -- new table for the hero
    player.grid_x = 256
    player.grid_y = 256
    player.act_x = 200
    player.act_y = 200
    player.size_x = 32
    player.size_y = 32
    player.speed = 10
    player.move_direction = "neutral"
    player.big = false
    player.turnsSinceSmall = 0
    player.startTurnCount = false

    -- 2.table.box:
    box = {} -- new table for the hero
    box.grid_x = 544
    box.grid_y = 544
    box.act_x = 500
    box.act_y = 500
    box.size_x = 32
    box.size_y = 32
    box.speed = 10
    box.big = false

    -- 3.table.levelexit
    levelexit = {}
    levelexit.grid_x = 960
    levelexit.grid_y = 384
    levelexit.act_x = 900
    levelexit.act_y = 300
    levelexit.speed = 10

    -- 4.table.sensor
    sensor = {}
    sensor.grid_x = 544
    sensor.grid_y = 384
    sensor.act_x = 500
    sensor.act_y = 300
    sensor.speed = 10

    -- 5.table.door
    door = {}
    door.grid_x = 928
    door.grid_y = 384
    door.act_x = 400
    door.act_y = 300
    door.speed = 10
    door.unlocked  = false


    -- 6.table.pellet (size powerup)
    pellet = {}
    pellet.grid_x = 832 -- + 8
    pellet.grid_y = 480 -- + 8
    pellet.act_x = 800
    pellet.act_y = 400
    pellet.speed = 10
    pellet.exists = true
end

function playerBigExpires()
    if startTurnCount == true then
        if turnsSinceSmall > 10 then
            player.big = false
            startTurnCount = false
            turnsSinceSmall = 0
        end
    end
end

--  ==========================| Collision Code! (ick :P) |==========================

-- Format: test_MovingObj_staticObj, so: testPlayerBox

--          Execution Code:

function CollisionExecution()
    testPlayerBox()
    handlePlayerBox()

    testPlayerExit()
    handlePlayerExit()

    testBoxSensor()
    handleBoxSensor()

    testPlayerDoor()
    handlePlayerDoor()

    testPlayerPellet()
    handlePlayerPellet()
end


--          Testing Code:

-- Here is the original collision test code in java/processing 1.5, it explains a lot:
-- if (abs(boxPosX-playerX) <= playersize/2 + boxSizeX/2 && abs(boxPosY-playerY) <= playersize/2 + boxSizeY/2)

function testPlayerBox()
        if math.abs(box.grid_x - player.grid_x) <= box.size_x/2 + player.size_x/2 and math.abs(box.grid_y - player.grid_y) <= box.size_y/2 + player.size_y/2 then
             return true
        else
             return false
        end
end

function testPlayerExit()
    if math.abs(levelexit.grid_x - player.grid_x) <= 32/2 + player.size_x/2 and math.abs(levelexit.grid_y - player.grid_y) <= 32/2 + player.size_y/2 then
         return true
      else
         return false
    end
end

function testBoxSensor()
    if math.abs(sensor.grid_x - box.grid_x) <= 32/2 + box.size_x/2 and math.abs(sensor.grid_y - box.grid_y) <= 32/2 + box.size_y/2 then
         return true
      else
         return false
    end
end

function testPlayerDoor()
    if math.abs(player.grid_x - door.grid_x) <= 32/2 + 32/2 and math.abs(player.grid_y - door.grid_y) <= 32/2 + 32/2 then
         return true
      else
         return false
    end
end

function testPlayerPellet()
    if math.abs(player.grid_x - pellet.grid_x) <= 32/2 + 32/2 and math.abs(player.grid_y - pellet.grid_y) <= 32/2 + 32/2 then
         return true
      else
         return false
    end
end

--          Handling Code:

-- format as follows: handleMovingObjstaticObj, so: handlePlayerBox

function handlePlayerBox()
    if testPlayerBox() == true then
        if box.big == false then
            if player.move_direction == "up" then
                if testMapBox(0, -1) then
                    box.grid_y = box.grid_y - 32
                end
            elseif player.move_direction == "down" then
                if testMapBox(0, 1) then
                    box.grid_y = box.grid_y + 32
                end
            elseif player.move_direction == "left" then
                if testMapBox(-1, 0) then
                    box.grid_x = box.grid_x - 32
                end
            elseif player.move_direction == "right" then
                if testMapBox(1, 0) then
                    box.grid_x = box.grid_x + 32
                end
            end
        end
    elseif box.big == true and player.big == true then
        if player.move_direction == "up" then
            if testMapBox(0, -1) then
                box.grid_y = box.grid_y - 32
            end
        elseif player.move_direction == "down" then
            if testMapBox(0, 1) then
                box.grid_y = box.grid_y + 32
            end
        elseif player.move_direction == "left" then
            if testMapBox(-1, 0) then
                box.grid_x = box.grid_x - 32
            end
        elseif player.move_direction == "right" then
            if testMapBox(1, 0) then
                box.grid_x = box.grid_x + 32
            end
        end
    end
end

function handlePlayerExit()
    while testPlayerExit() == true do -- it definitely registers this collision...
            map.currentLevel = map.currentLevel + 1
            player.grid_x = 256
            player.grid_y = 256
            player.act_x = 200
            player.act_y = 200
            break
    end
end


function handleBoxSensor()
    if testBoxSensor() == true then
        door.unlocked = true
    end
    if testBoxSensor() == false then
        door.unlocked = false
    end
end


function handlePlayerDoor()
    if testPlayerDoor() == true and door.unlocked == false then
        if player.move_direction == "up" then
            player.grid_y = player.grid_y + 32
        elseif player.move_direction == "down" then
            player.grid_y = player.grid_y - 32
        elseif player.move_direction == "left" then
            player.grid_x = player.grid_x + 32
        elseif player.move_direction == "right" then
            player.grid_x = player.grid_x - 32
        end
    end
end

function handlePlayerPellet()
    if testPlayerPellet() == true then
        if pellet.exists == true then
            player.big = true
            player.startTurnCount = true
            pellet.exists = false
        end
    end
end






